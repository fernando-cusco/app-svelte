FROM node:14-alpine
WORKDIR /usr/src/app
COPY rollup.config.js ./
COPY package*.json ./
RUN npm install
COPY ./src ./src
COPY ./public ./public
RUN npm run-script build
EXPOSE 5000
ENV HOST=0.0.0.0
CMD [ "npm", "start" ]

#docker run -d -it app-svelte -p 5000:5000